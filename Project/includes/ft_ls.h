/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/20 15:21:02 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/18 18:29:15 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# define XATTR_SIZE 10000
# define PATH_MAX 4096
# define CYAN "\033[36m"
# define MAGENTA "\033[95m"
# define NANOSEC st_mtimespec.tv_nsec
# define MYISDIR S_ISDIR(node->info.sb.st_mode
# define MODE(st, z) (( st & (z)) == (z))
# define DUPATH(h, p) (ft_strdup(ft_strjoin(ft_strjoin(h, "/"), p)))
# define ROOT ft_strcmp(info->name, "..") && ft_strcmp(info->name, ".")
# define ASC ft_strcmp((*tree)->info.name, info.name) > 0
# define DSC ft_strcmp((*tree)->info.name, info.name) < 0
# define ASCT info.sb.st_mtime > (*tree)->info.sb.st_mtime
# define DSCT info.sb.st_mtime < (*tree)->info.sb.st_mtime
# define NASCT info.sb.NANOSEC > (*tree)->info.sb.NANOSEC
# define NDSCT info.sb.NANOSEC < (*tree)->info.sb.NANOSEC
# define LNK_PARAMS t_files **files, t_files **dir, t_files *lnk, t_flags *fl
# define SORT_BY_TYPE_STATEMENT !(tmp = opendir(node->info.name)) && !MYISDIR)

# include <stdbool.h>
# include <stdio.h>
# include <sys/ioctl.h>
# include <sys/stat.h>
# include <pwd.h>
# include <grp.h>
# include <dirent.h>
# include <errno.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <string.h>
# include "libft.h"

typedef struct s_flags	t_flags;
typedef struct s_info	t_info;
typedef struct s_files	t_files;

struct			s_info
{
	char		*name;
	char		*path;
	long		link_count;
	char		*error;
	int			list_size;
	long		total_size;
	int			size;
	struct stat	sb;
};

struct			s_files
{
	t_info		info;
	t_files		*right;
	t_files		*left;
};

struct			s_flags
{
	int			op_l;
	int			op_br;
	int			op_a;
	int			op_r;
	int			op_t;
	int			op_1;
	int			counter;
	int			ac;
	char		*head;
};

int				get_data_files(char **av, t_files **files, \
					char *buff, t_flags *fl);
void			get_data(t_info *file, char *name_file, char *buff);
void			ft_print_dir(t_files *aux, t_flags *flags, char *buff);
void			ft_print_link(t_files *aux, t_flags *flags);
char			*read_link_path(t_files **lnk_name);
char			**ft_flags_purge(char **src);
void			display(t_files **tree, t_flags *flags, \
					t_files **files, t_files **dir);
void			display_basic_flags(t_files **tree,\
					t_flags *flags, char *buff);
void			r_content(t_files **files, t_flags *flags, \
					char *buff, char *pth);
void			op_recursion(t_files **tree, t_flags *flags, char *buff);
void			recursive_path(t_files *tree, t_flags *flags);
void			display_dir(t_files **files, t_flags *flags, \
					char *buff, int bl);
void			list_content_dir(t_flags *flags, char *buff, char *pth, int b);
void			arg_content(t_files **files, t_flags *flags, \
					char *buff, char **av);

/*
** files treatement
*/

int				clasify_file(t_files **aux, t_list **dir_l, t_flags *flags);
int				clasify_link(t_files **aux, t_list **dir_l, t_flags *flags);
void			ft_get_lnk(t_files **files, t_files **dir, t_files *lnk, \
					t_flags *fl);
char			*read_link(t_files **lnk);
void			sort_by_type(t_files **dir, t_files **files, \
					t_files *node, t_flags *fl);

/*
** init memory to null
*/

void			init_flags(t_flags **flags, int ac);

/*
** tree sort FUNCTIONS
*/

void			ft_comp_date(t_files **tree, t_info info, t_flags *fl);
t_files			*ft_new_node(t_info data);
void			ft_insert(t_files **tree, t_info info, t_flags *fl);
void			ft_delete_tnode(t_files **tree, char *s);
int				ft_btsize(t_files *node);
void			ft_delete_all(t_files **tree);
void			ft_deallocate(t_files *leaf);
void			ft_dalloc_tree(t_files **tree);
void			ft_add_node(t_files **tree, t_info info, t_flags *fl);

/*
** flags
*/

t_flags			get_flags(char *options, int ac, char **av);
void			set_flags(t_flags *flags, char *options);
int				check_type_file(char *file);

/*
** Errors
*/

int				check_flag_error(int ac, char **files, t_flags *flags);
void			ls_error(t_files *e);
void			ft_kill_errors(char **files);
char			**search_index(char **src);

/*
** Options
*/

void			set_basic_list(t_files **tree, char *path, \
					char *buff, t_flags *fl);
void			set_data(t_files **root, char *file, char *buff, t_flags *fl);
char			*get_mode_format(mode_t st_mode);
char			get_type_file(mode_t st_mode);
void			ft_put_permisions(t_flags *flags, t_info file);
void			p_ocases(t_files *tree, t_flags *flags, char *buff);

/*
** OP_L
*/

void			print_bzise(t_files **tree, t_flags *flags);
void			print_posix(t_flags *flags, t_info *file, char *buff);
int				get_total_bzise(t_files **tree, t_flags *flags);

#endif
