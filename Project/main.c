/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/14 11:12:01 by jbernabe          #+#    #+#             */
/*   Updated: 2016/06/20 16:26:26 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include "ft_ls.h"
#include "libft.h"

void		r_content(t_files **files, t_flags *flags, char *buff, char *pth)
{
	ft_bzero(buff, ft_strlen(buff));
	set_basic_list(files, pth, buff, flags);
	if (flags->op_l == 1)
	{
		print_bzise(files, flags);
	}
	display_basic_flags(files, flags, buff);
}

void		arg_content(t_files **files, t_flags *flags, char *buff, char **av)
{
	if (get_data_files(av, files, buff, flags) == 0)
	{
		r_content(files, flags, buff, ".");
		if (flags->op_br == 1)
		{
			flags->head = ft_strdup(".");
			op_recursion(files, flags, buff);
			free(flags->head);
		}
		exit(0);
	}
}

void		get_dir_files(t_files **dir, t_flags **flags, char *buff, int len)
{
	int	dir_len;

	dir_len = ft_btsize(*dir);
	if (len > 0)
	{
		ft_putstr("\n");
	}
	display_dir(dir, *flags, buff, dir_len);
	ft_dalloc_tree(dir);
	free(*flags);
}

int			main(int ac, char **argv)
{
	t_files	*files;
	t_flags	*flags;
	t_files	*reg_files;
	t_files	*dir;
	char	buff[5];

	files = NULL;
	reg_files = NULL;
	dir = NULL;
	ft_bzero(buff, 5);
	init_flags(&flags, ac);
	check_flag_error(ac, argv, flags);
	arg_content(&files, flags, buff, argv);
	display(&files, flags, &reg_files, &dir);
	ft_dalloc_tree(&files);
	display_basic_flags(&reg_files, flags, buff);
	ft_dalloc_tree(&reg_files);
	get_dir_files(&dir, &flags, buff, ft_btsize(reg_files));
	return (0);
}
