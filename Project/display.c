/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/11 16:31:42 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/11 16:31:44 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "ft_ls.h"

char	*read_link_path(t_files **lnk)
{
	ssize_t		r;
	char		*buff;
	t_info		*aux;

	aux = &(*lnk)->info;
	if ((aux->sb.st_mode & S_IFMT) != S_IFLNK)
		return ((*lnk)->info.path);
	buff = (char *)ft_memalloc(aux->sb.st_size + 1);
	r = readlink((const char *)aux->path, buff, aux->sb.st_size + 1);
	if (r < 0)
	{
		aux->error = ft_strdup(strerror(errno));
		(*lnk)->info.path = ft_strdup(buff);
		free(buff);
		return ((*lnk)->info.path);
	}
	else
	{
		buff[aux->sb.st_size] = '\0';
		(*lnk)->info.path = ft_strdup(buff);
		aux->error = ft_strdup(strerror(errno));
		free(buff);
		return ((*lnk)->info.path);
	}
}

char	*read_link(t_files **lnk)
{
	ssize_t		r;
	char		*buff;
	t_info		*aux;

	aux = &(*lnk)->info;
	if (S_ISLNK(aux->sb.st_mode))
		return ((*lnk)->info.path);
	buff = (char *)malloc(aux->sb.st_size + 1);
	r = readlink((const char *)aux->name, buff, aux->sb.st_size + 1);
	if (r < 0)
	{
		aux->error = ft_strdup(strerror(errno));
		(*lnk)->info.path = ft_strdup(buff);
		free(buff);
		return ((*lnk)->info.path);
	}
	else
	{
		buff[(*lnk)->info.sb.st_size] = '\0';
		(*lnk)->info.error = ft_strdup(strerror(errno));
		(*lnk)->info.path = ft_strdup(buff);
		free(buff);
		return (errno ? ft_itoa(errno) : (*lnk)->info.path);
	}
}

void	display_basic_flags(t_files **tree, t_flags *flags, char *buff)
{
	t_info	*aux;
	t_files	*tmp;

	tmp = *tree;
	aux = &tmp->info;
	if (!tmp)
	{
		return ;
	}
	display_basic_flags(&tmp->left, flags, buff);
	if (ft_str_hidden_path(aux->name) == 0)
	{
		(flags->op_l == 1) ? ft_print_dir(*tree, flags, buff) : \
		ft_putendl(aux->name);
	}
	else if (flags->op_a == 1 && ft_str_hidden_path(aux->name) == 1)
	{
		print_posix(flags, aux, buff);
		ft_putendl(aux->name);
	}
	display_basic_flags(&tmp->right, flags, buff);
}

void	display(t_files **tree, t_flags *flags, t_files **files, t_files **dir)
{
	t_files	*tmp;

	tmp = *tree;
	if (!tmp)
	{
		return ;
	}
	display(&tmp->left, flags, files, dir);
	ft_get_lnk(files, dir, tmp, flags);
	sort_by_type(dir, files, tmp, flags);
	display(&tmp->right, flags, files, dir);
}
