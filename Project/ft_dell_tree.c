/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dell_tree.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 11:03:17 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 11:03:19 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void		ft_replace(t_files **tree, t_files **aux)
{
	if ((*tree)->right != NULL)
	{
		ft_replace(&(*tree)->right, aux);
	}
	else
	{
		(*aux)->info.name = ft_strdup((char *)(*tree)->info.name);
		*aux = *tree;
		*tree = (*tree)->left;
	}
}

void			ft_delete_tnode(t_files **tree, char *s)
{
	t_files	*tmp;

	tmp = NULL;
	if (*tree == NULL)
		return ;
	if (ft_strcmp((*tree)->info.name, s) < 0)
	{
		ft_delete_tnode(&(*tree)->right, s);
	}
	else if (ft_strcmp((*tree)->info.name, s) > 0)
	{
		ft_delete_tnode(&(*tree)->left, s);
	}
	else if (ft_strcmp((*tree)->info.name, s) == 0)
	{
		tmp = *tree;
		if ((*tree)->left == NULL)
			*tree = (*tree)->right;
		else if ((*tree)->right == NULL)
			*tree = (*tree)->left;
		else
			ft_replace(&(*tree)->left, &tmp);
	}
}

void			ft_dalloc_tree(t_files **tree)
{
	t_files	*tmp;

	tmp = *tree;
	if (!tmp)
	{
		return ;
	}
	ft_dalloc_tree(&(tmp)->left);
	ft_dalloc_tree(&(tmp)->right);
	free(tmp);
	tmp = NULL;
}

void			ft_deallocate(t_files *leaf)
{
	t_info	*info;

	info = &leaf->info;
	free(info->path);
	leaf = NULL;
}
