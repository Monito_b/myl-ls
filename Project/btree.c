/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/11 16:33:22 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/18 18:33:16 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <libft.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include "ft_ls.h"

t_files		*ft_new_node(t_info data)
{
	t_files		*node;
	static int	i;

	node = (t_files*)malloc(sizeof(t_files));
	if (!node)
	{
		return (NULL);
	}
	node->info = data;
	node->left = NULL;
	node->right = NULL;
	i++;
	return (node);
}

int			ft_btsize(t_files *node)
{
	if (node == NULL)
	{
		return (0);
	}
	else
	{
		return (ft_btsize(node->left) + 1 + ft_btsize(node->right));
	}
}

void		ft_add_node(t_files **tree, t_info info, t_flags *fl)
{
	if (*tree == NULL)
		*tree = ft_new_node(info);
	else if (fl->op_t == 1)
	{
		if ((fl->op_r == 1) ? DSCT : ASCT)
		{
			ft_add_node(&(*tree)->left, info, fl);
		}
		else if (info.sb.st_mtime == (*tree)->info.sb.st_mtime)
		{
			if ((fl->op_r == 1) ? NDSCT : NASCT)
				ft_add_node(&(*tree)->left, info, fl);
			else
				ft_add_node(&(*tree)->right, info, fl);
		}
		else
		{
			ft_add_node(&(*tree)->right, info, fl);
		}
	}
	else
		return (ft_insert(tree, info, fl));
}

void		ft_insert(t_files **tree, t_info info, t_flags *fl)
{
	if (*tree == NULL)
	{
		*tree = ft_new_node(info);
	}
	else
	{
		if ((fl->op_r) ? DSC : ASC)
		{
			ft_insert(&((*tree))->left, info, fl);
		}
		else
		{
			ft_insert(&((*tree))->right, info, fl);
		}
	}
}
