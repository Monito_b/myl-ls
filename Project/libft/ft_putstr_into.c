/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_into.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/20 13:00:21 by jbernabe          #+#    #+#             */
/*   Updated: 2016/06/20 13:05:13 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_putstr_into(char *space, char *s, char *end_space)
{
	ft_putstr(space);
	ft_putstr(s);
	ft_putstr(end_space);
}
