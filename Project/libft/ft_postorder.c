/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_postorder.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 11:43:57 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 11:44:01 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_postorder(t_tree *n)
{
	if (n != NULL)
	{
		ft_postorder(n->left);
		ft_postorder(n->right);
		ft_putendl((char *)n->data);
	}
}
