/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_max_nlnk.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 12:23:07 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 12:53:45 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	set_max_nlnk(char *buff, int n_lnk)
{
	int		size;

	size = ft_nbrlen(n_lnk);
	if (size > buff[3])
	{
		buff[3] = size;
	}
}
