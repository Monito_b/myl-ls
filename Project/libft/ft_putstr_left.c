/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_left.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/12 06:05:53 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/12 06:05:55 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_putstr_left(char *s, const int max_size)
{
	int		len;

	len = ft_strlen(s) - 1;
	ft_putstr(s);
	ft_print_space(max_size - len);
}
