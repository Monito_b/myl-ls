/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_max_guid_len.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/11 16:32:14 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 12:51:56 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
**	set_max_gui_len:
**	get the size of string
**	set the max value between of buff[1]
*/

void		set_max_guid_len(char *buff, char *name)
{
	int		size;

	size = ft_strlen(name);
	if (size > buff[1])
		buff[1] = size;
}
