/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_from_back_split.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 14:41:48 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 14:55:24 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** UTILITY FUNCTIONS
** Split the nodes of the given list into front and back halves,
** and return the two lists using the reference parameters.
** If the length is odd, the extra node should go in the front list.
** Uses the fast/slow pointer strategy.
** line 20: length < 2 cases
** line 29: Advance 'fast' two nodes, and advance 'slow' one node
** line 36: 'slow' is before the midpoint in the list,
** so split it in two at that point.
*/

void		ft_from_back_split(t_list *src, t_list **front_r, t_list **back_r)
{
	t_list		*fast;
	t_list		*slow;

	if (src == NULL || src->next == NULL)
	{
		*front_r = src;
		*back_r = NULL;
	}
	else
	{
		slow = src;
		fast = (src)->next;
		while (fast != NULL)
		{
			fast = fast->next;
			if (fast != NULL)
			{
				slow = slow->next;
				fast = fast->next;
			}
		}
		*front_r = src;
		*back_r = slow->next;
		slow->next = NULL;
	}
}
