/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree_size.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 10:58:37 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 10:59:13 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_btree_size(t_tree *node)
{
	if (node == NULL)
	{
		return (0);
	}
	else
	{
		return (ft_btree_size((node)->left) + 1 + ft_btree_size((node)->right));
	}
}
