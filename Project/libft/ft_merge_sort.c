/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_merge_sort.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 15:23:23 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 15:23:29 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"

/*
** ft_merge_sort
** line 18: Split head into 'a' and 'b' sublists
** line 24: answer = merge the two sorted lists together
*/

void		ft_merge_sort(t_list **head)
{
	t_list	*head_t;
	t_list	*a;
	t_list	*b;

	head_t = *head;
	if (!head_t || head_t->next == NULL)
	{
		return ;
	}
	ft_from_back_split(head_t, &a, &b);
	ft_merge_sort(&a);
	ft_merge_sort(&b);
	*head = ft_sorted_merge(a, b);
	ft_memdel((void **)&a);
	ft_memdel((void **)&b);
}
