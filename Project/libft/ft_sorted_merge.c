/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sorted_merge.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 15:21:41 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 15:22:04 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** ft_sort_merge:
** Base cases line 14 - 21: check if first list exist or b exist
** return the list list which exist
** Pick either a or b, and recursion to sort
*/

t_list		*ft_sorted_merge(t_list *a, t_list *b)
{
	t_list	*result;

	result = NULL;
	if (!a)
	{
		return (b);
	}
	else if (!b)
	{
		return (a);
	}
	if (ft_strcmp(a->content, b->content) < 0)
	{
		result = a;
		result->next = ft_sorted_merge(a->next, b);
	}
	else
	{
		result = b;
		result->next = ft_sorted_merge(a, b->next);
	}
	return (result);
}
