/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew_map.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 14:30:04 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 14:30:07 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		***ft_strnew_map(int height, int wight)
{
	char	***map;
	int		i;

	i = 0;
	map = (char ***)ft_memalloc(sizeof(char **) * (wight + 1));
	if (!map)
		return (NULL);
	else
	{
		while (i < height)
		{
			map[i] = (char **)ft_memalloc(sizeof(char *) * height + 1);
			i++;
		}
	}
	return (map);
}
