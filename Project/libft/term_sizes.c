/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   term_sizes.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 12:38:36 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 12:51:00 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <sys/ioctl.h>

struct winsize	term_sizes(void)
{
	struct winsize	ws;
	int				havewin;

	havewin = 0;
	ws.ws_row = 10;
	ws.ws_col = 2;
	if (isatty(STDIN_FILENO))
	{
		havewin = ioctl(STDIN_FILENO, TIOCGWINSZ, &ws) != -1;
	}
	return (ws);
}
