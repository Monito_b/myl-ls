/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 12:11:36 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 12:11:39 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_realloc(void *ptr, size_t size)
{
	char	*tmp;
	size_t	n;

	tmp = NULL;
	if (ptr != NULL)
	{
		n = ft_strlen((char *)ptr);
		if ((tmp = ft_memalloc(n + size + 1)) == NULL)
			return (NULL);
		ft_memcpy(tmp, ptr, n);
		ft_memdel(&ptr);
	}
	return (tmp);
}
