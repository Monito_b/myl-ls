/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew_grid.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 14:25:56 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 14:26:00 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft.h"

char		**ft_strnew_grid(int wigth, int height)
{
	char	**map;
	int		i;

	map = ft_memalloc(sizeof(char **) * height + 1);
	map[height + 1] = NULL;
	i = 0;
	while (map[i] != NULL)
	{
		map[i] = ft_memalloc(sizeof(char **) * wigth);
		i++;
	}
	return (map);
}
