/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_max_nbr_len.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/11 16:32:43 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/11 16:32:44 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	set_max_nbr_len(char *buff, int size_file)
{
	int		size;

	size = ft_nbrlen(size_file);
	if (size > buff[2])
		buff[2] = size;
}
