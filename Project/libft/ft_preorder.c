/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_preorder.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 11:43:25 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 11:43:28 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_preorder(t_tree *n)
{
	if (n != NULL)
	{
		ft_putendl((char *)n->data);
		ft_preorder(n->left);
		ft_preorder(n->right);
	}
}
