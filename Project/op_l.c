/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op_l.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/11 16:33:53 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/11 16:33:55 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "ft_ls.h"

void		print_bzise(t_files **tree, t_flags *flags)
{
	int	bl;
	int	len_tree;

	bl = 0;
	len_tree = ft_btsize(*tree);
	if (len_tree >= 2 && flags->op_a == 1)
		bl = 1;
	else if (len_tree > 2)
		bl = 1;
	if (*tree)
	{
		if ((bl == 1 && flags->op_l == 1)
			|| (flags->op_l == 1 && flags->op_a == 1 && len_tree >= 2))
		{
			ft_putstr("total ");
			ft_putnbr_endl(get_total_bzise(tree, flags));
		}
	}
	return ;
}

int			get_total_bzise(t_files **tree, t_flags *flags)
{
	int		total;
	t_files	*tmp;
	t_info	*info;

	total = 0;
	tmp = *tree;
	info = &tmp->info;
	if (tmp && flags->op_l == 1)
	{
		if (tmp->left != NULL)
			total += get_total_bzise(&(tmp)->left, flags);
		if (info->link_count == 0)
			ft_delete_tnode(tree, info->name);
		if (str_search_cl(info->name, 2, '.') == 1 && flags->op_a == 1)
		{
			total += info->size;
		}
		else if (str_search_cl(info->name, 2, '.') == 0)
		{
			total += info->size;
		}
		if (tmp->right != NULL)
			total += get_total_bzise(&tmp->right, flags);
	}
	return (total);
}

void		ft_print_dir(t_files *dir, t_flags *flags, char *buff)
{
	t_info	*tmp;

	tmp = &dir->info;
	if (flags->op_l == 1)
	{
		print_posix(flags, tmp, buff);
	}
	if ((tmp->sb.st_mode & S_IFMT) != S_IFLNK)
	{
		if (S_ISDIR(tmp->sb.st_mode))
			ft_putendl(tmp->name);
		else
			ft_putendl(tmp->name);
	}
	else
	{
		ft_print_link(dir, flags);
	}
}

void		ft_print_link(t_files *lnk, t_flags *flags)
{
	t_info	*tmp;

	tmp = &lnk->info;
	if (flags->op_l == 1)
	{
		read_link(&lnk);
		ft_putstr(lnk->info.path);
		ft_putstr_into(" ", "->", " ");
		ft_putendl(read_link_path(&lnk));
	}
	else
	{
		read_link(&lnk);
		if (lstat(lnk->info.path, &tmp->sb))
		{
			perror(lnk->info.path);
			return ;
		}
		ft_putstr_color(MAGENTA, tmp->name);
		tmp->error = strerror(errno);
		ft_deallocate(lnk);
	}
	return ;
}
